// Package main is the main program.
package main

import "gitlab.com/lyda/template-go-api/cmd"

func main() {
	cmd.Execute()
}
