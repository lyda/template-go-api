// Package cmd containe the cli commands.
package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/lyda/template-go-api/internal/server"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "template-go-api",
	Short: "Template Go application to implement an API",
	Long: `This application implements a template for an API
server that is generated from an OpenAPI document.

This reduces the amount of code being written and
greatly simplifies implementing an API server.`,
	Run: server.Run,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringP("listen", "l", ":10721", "Listen address:port")
}
