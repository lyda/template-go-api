FROM golang:alpine as builder

# Included only for /etc/ssl/certs/ca-certificates.crt

FROM scratch

COPY ./bin/template-go-api /template-go-api

# Need the root certs to talk to external sites.
# TODO: Is there a smaller container for these?  Or maybe a go module
#       for them?
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER 10000

ENTRYPOINT ["/template-go-api"]
