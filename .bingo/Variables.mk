# Auto generated binary variables helper managed by https://github.com/bwplotka/bingo v0.8. DO NOT EDIT.
# All tools are designed to be build inside $GOBIN.
BINGO_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
GOPATH ?= $(shell go env GOPATH)
GOBIN  ?= $(firstword $(subst :, ,${GOPATH}))/bin
GO     ?= $(shell which go)

# Below generated variables ensure that every time a tool under each variable is invoked, the correct version
# will be used; reinstalling only if needed.
# For example for cobra-cli variable:
#
# In your main Makefile (for non array binaries):
#
#include .bingo/Variables.mk # Assuming -dir was set to .bingo .
#
#command: $(COBRA_CLI)
#	@echo "Running cobra-cli"
#	@$(COBRA_CLI) <flags/args..>
#
COBRA_CLI := $(GOBIN)/cobra-cli-v1.3.0
$(COBRA_CLI): $(BINGO_DIR)/cobra-cli.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/cobra-cli-v1.3.0"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=cobra-cli.mod -o=$(GOBIN)/cobra-cli-v1.3.0 "github.com/spf13/cobra-cli"

GOCOVER_COBERTURA := $(GOBIN)/gocover-cobertura-v1.2.0
$(GOCOVER_COBERTURA): $(BINGO_DIR)/gocover-cobertura.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/gocover-cobertura-v1.2.0"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=gocover-cobertura.mod -o=$(GOBIN)/gocover-cobertura-v1.2.0 "github.com/boumenot/gocover-cobertura"

GOLANGCI_LINT := $(GOBIN)/golangci-lint-v1.55.1
$(GOLANGCI_LINT): $(BINGO_DIR)/golangci-lint.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/golangci-lint-v1.55.1"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=golangci-lint.mod -o=$(GOBIN)/golangci-lint-v1.55.1 "github.com/golangci/golangci-lint/cmd/golangci-lint"

GOTESTSUM := $(GOBIN)/gotestsum-v1.11.0
$(GOTESTSUM): $(BINGO_DIR)/gotestsum.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/gotestsum-v1.11.0"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=gotestsum.mod -o=$(GOBIN)/gotestsum-v1.11.0 "gotest.tools/gotestsum"

GOVULNCHECK := $(GOBIN)/govulncheck-v1.0.1
$(GOVULNCHECK): $(BINGO_DIR)/govulncheck.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/govulncheck-v1.0.1"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=govulncheck.mod -o=$(GOBIN)/govulncheck-v1.0.1 "golang.org/x/vuln/cmd/govulncheck"

HELM := $(GOBIN)/helm-v3.13.1
$(HELM): $(BINGO_DIR)/helm.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/helm-v3.13.1"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=helm.mod -o=$(GOBIN)/helm-v3.13.1 "helm.sh/helm/v3/cmd/helm"

KIND := $(GOBIN)/kind-v0.20.0
$(KIND): $(BINGO_DIR)/kind.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/kind-v0.20.0"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=kind.mod -o=$(GOBIN)/kind-v0.20.0 "sigs.k8s.io/kind"

MOCKERY := $(GOBIN)/mockery-v2.33.3
$(MOCKERY): $(BINGO_DIR)/mockery.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/mockery-v2.33.3"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=mockery.mod -o=$(GOBIN)/mockery-v2.33.3 "github.com/vektra/mockery/v2"

OAPI_CODEGEN := $(GOBIN)/oapi-codegen-v2.0.1-0.20231213133025-4ceb1d567792
$(OAPI_CODEGEN): $(BINGO_DIR)/oapi-codegen.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/oapi-codegen-v2.0.1-0.20231213133025-4ceb1d567792"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=oapi-codegen.mod -o=$(GOBIN)/oapi-codegen-v2.0.1-0.20231213133025-4ceb1d567792 "github.com/deepmap/oapi-codegen/v2/cmd/oapi-codegen"

STATICCHECK := $(GOBIN)/staticcheck-v0.4.6
$(STATICCHECK): $(BINGO_DIR)/staticcheck.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/staticcheck-v0.4.6"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=staticcheck.mod -o=$(GOBIN)/staticcheck-v0.4.6 "honnef.co/go/tools/cmd/staticcheck"

VACUUM := $(GOBIN)/vacuum-v0.6.3
$(VACUUM): $(BINGO_DIR)/vacuum.mod
	@# Install binary/ries using Go 1.14+ build command. This is using bwplotka/bingo-controlled, separate go module with pinned dependencies.
	@echo "(re)installing $(GOBIN)/vacuum-v0.6.3"
	@cd $(BINGO_DIR) && GOWORK=off $(GO) build -mod=mod -modfile=vacuum.mod -o=$(GOBIN)/vacuum-v0.6.3 "github.com/daveshanley/vacuum"

