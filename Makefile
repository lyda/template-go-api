# Settings:
CGO_ENABLED=0
GO_BUILD_TAGS=
GO_CI_BUILD_TAGS=
BIN=template-go-api
LABEL=latest
CONTAINER_PATH=ecr-path-goes-here/$(BIN)
CONTAINER_LABEL=$(CI_PIPELINE_IID)
KIND_CLUSTER=kind
KUBECTL=kubectl
NAMESPACE=template-go-api
APP_NAME=template-go-api

.PHONY: all ci-all generate ci-generate lint ci-lint test ci-test build \
	ci-build container ci-container create-kind-cluster deploy-kind \
	delete-kind-cluster cobra-cli kubectl


all: generate test lint build

ci-all: ci-generate ci-test ci-lint ci-build

include .bingo/Variables.mk

generate: $(MOCKERY) $(OAPI_CODEGEN)
	@MOCKERY=$(MOCKERY) OAPI_CODEGEN=$(OAPI_CODEGEN) \
	    go generate ./...

ci-generate: $(MOCKERY) $(OAPI_CODEGEN)
	MOCKERY=$(MOCKERY) OAPI_CODEGEN=$(OAPI_CODEGEN) \
	    go generate -v ./...

lint: $(GOLANGCI_LINT)
	$(GOLANGCI_LINT) run

ci-lint: $(GOLANGCI_LINT) reports
	$(GOLANGCI_LINT) run --out-format code-climate > reports/lint.json

test: $(GOTESTSUM) $(GOVULNCHECK) reports
	yamllint api/api.yaml
	go fmt ./...
	go vet ./...
	$(GOVULNCHECK) ./...
	CGO_ENABLED=1 go test -race ./...
	$(GOTESTSUM) --junitfile reports/coverage.xml --format testname -- \
		-coverprofile=reports/coverage.txt -covermode=count ./...
	go tool cover -func reports/coverage.txt
	go tool cover -html=reports/coverage.txt

ci-test: $(GOTESTSUM) $(GOCOVER_COBERTURA) $(GOVULNCHECK) reports
	yamllint api/api.yaml
	go fmt ./...
	go vet ./...
	$(GOVULNCHECK) -json ./... > reports/govulncheck.json
	CGO_ENABLED=1 go test -race ./...
	$(GOTESTSUM) --junitfile reports/gotestsum.xml --format testname -- \
		-coverprofile=reports/coverage.txt -covermode=count ./...
	go tool cover -func reports/coverage.txt
	go tool cover -html=reports/coverage.txt
	$(GOCOVER_COBERTURA) < reports/coverage.txt > reports/coverage.xml

build:
	@CGO_ENABLED=$(CGO_ENABLED) go build $(GO_BUILD_TAGS) -o ./bin/$(BIN)

ci-build:
	CGO_ENABLED=$(CGO_ENABLED) go build $(GO_CI_BUILD_TAGS) -o ./bin/$(BIN)

container: $(KIND) build
	@docker build -t $(BIN) .
	@$(KIND) load --name $(KIND_CLUSTER) docker-image $(BIN):latest

ci-container:
	/kaniko/executor \
		--cache=true --context "$(PWD)" \
		--destination "$(CONTAINER_PATH):$(CONTAINER_LABEL)"

	docker build -t $(BIN):$(LABEL) .

# Utilities.

create-kind-cluster: $(KIND) $(KUBECTL)
	@if ! $(KIND) get clusters | grep '^$(KIND_CLUSTER)$$' > /dev/null; then \
	  $(KIND) create cluster --name $(KIND_CLUSTER) \
	                         --config=deploy/kind/config.yaml; \
		$(KUBECTL) --context kind-$(KIND_CLUSTER) apply -f \
		             https://projectcontour.io/quickstart/contour.yaml; \
  fi

deploy-kind: $(KIND) $(HELM) $(KUBECTL)
	@if $(KUBECTL) --context kind-$(KIND_CLUSTER) \
	      get deploy -n "$(NAMESPACE)" -o json \
		    | jq --arg app "$(APP_NAME)" -er \
				     '.items[]|.metadata.name|select(.==$$app)' > /dev/null; then \
		$(HELM) --kube-context kind-$(KIND_CLUSTER) \
		        upgrade -f deploy/$(APP_NAME)/values-kind.yaml \
		                --namespace $(NAMESPACE) \
		                $(APP_NAME) deploy/$(APP_NAME); \
	else \
		$(HELM) --kube-context kind-$(KIND_CLUSTER) \
		        install -f deploy/$(APP_NAME)/values-kind.yaml \
		                --create-namespace --namespace $(NAMESPACE) \
		                $(APP_NAME) deploy/$(APP_NAME); \
	fi
	@if $(KUBECTL) --context kind-$(KIND_CLUSTER) \
		   get deploy -n monitoring -o json \
		   | jq -er '.items[]|.metadata.name|select(.=="prometheus")' \
				 > /dev/null; then \
		$(HELM) upgrade -f deploy/prometheus/values-kind.yaml \
		                --namespace monitoring prometheus deploy/prometheus; \
	else \
		$(HELM) install -f deploy/prometheus/values-kind.yaml \
		                --create-namespace --namespace monitoring \
		                prometheus deploy/prometheus; \
	fi

delete-kind-cluster: $(KIND)
	$(KIND) delete cluster --name $(KIND_CLUSTER)

cobra-cli: $(COBRA_CLI)
	@if [ ! -f main.go ]; then \
		$(COBRA_CLI) init; \
	fi
	@if [ -n "$(CMD)" ]; then \
		$(COBRA_CLI) add $(CMD); \
	else \
	  echo 'Run "make cobra-cli CMD=command" to add a new command.'; \
	fi

reports:
	@mkdir reports

kubectl:
	@if ! command -v kubectl > /dev/null; then \
		echo "ERROR: kubectl not installed. See install instructions here:"; \
	  echo "       https://kubernetes.io/docs/tasks/tools/"; \
		exit 1; \
	fi
