// Package static provides an in memory filesystem.
package static

import (
	"embed"
	"io/fs"

	"github.com/spf13/cobra"
)

//go:embed files

var files embed.FS

// Files contains the static files to be served.
func Files() fs.FS {
	staticFs, err := fs.Sub(files, "files")
	cobra.CheckErr(err)
	return staticFs
}
