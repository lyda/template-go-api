// Package server implements the API server.
package server

import (
	"context"

	"gitlab.com/lyda/template-go-api/internal/api"
	"gitlab.com/lyda/template-go-api/internal/storage"
)

// API implements the api server.
type API struct {
	db storage.Storer
}

// NewAPI creates the API server.
func NewAPI(db storage.Storer) *API {
	return &API{db: db}
}

// AddPet adds a pet to the pet store.
func (a *API) AddPet(_ context.Context, req api.AddPetRequestObject) (api.AddPetResponseObject, error) {
	pet := req.Body
	id, err := a.db.AddPet(pet.Name, pet.Tag)
	if err != nil {
		return api.AddPetdefaultJSONResponse{
			Body: api.Error{
				Code:    500,
				Message: err.Error(),
			},
			StatusCode: 200,
		}, nil
	}
	return api.AddPet200JSONResponse{
		Id:   id,
		Name: pet.Name,
		Tag:  pet.Tag,
	}, nil
}

// DeletePet deletes a pet in the pet store.
func (a *API) DeletePet(_ context.Context, pet api.DeletePetRequestObject) (api.DeletePetResponseObject, error) {
	err := a.db.DeletePet(pet.Id)
	if err != nil {
		return api.DeletePetdefaultJSONResponse{
			Body: api.Error{
				Code:    500,
				Message: err.Error(),
			},
			StatusCode: 200,
		}, nil
	}
	return api.DeletePet204Response{}, nil
}

// FindPetByID finds a pet in the pet store by id.
func (a *API) FindPetByID(_ context.Context, pet api.FindPetByIDRequestObject) (api.FindPetByIDResponseObject, error) {
	found, err := a.db.FindPetByID(pet.Id)
	if err != nil {
		return api.FindPetByIDdefaultJSONResponse{
			Body: api.Error{
				Code:    500,
				Message: err.Error(),
			},
			StatusCode: 200,
		}, nil
	}
	return api.FindPetByID200JSONResponse(found), nil
}

// FindPets finds a pet in the pet store by id.
func (a *API) FindPets(_ context.Context, params api.FindPetsRequestObject) (api.FindPetsResponseObject, error) {
	pets, err := a.db.FindPets(params.Params)
	if err != nil {
		return api.FindPetsdefaultJSONResponse{
			Body: api.Error{
				Code:    500,
				Message: err.Error(),
			},
			StatusCode: 200,
		}, nil
	}
	return api.FindPets200JSONResponse(pets), nil
}
