// Package server implements the API server.
package server

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/lyda/template-go-api/internal/api"
	"gitlab.com/lyda/template-go-api/mock/ery"
)

var (
	dog = "dog"
	cat = "cat"
)

func setup(t *testing.T) (*ery.Storer, *echo.Echo) {
	db := ery.NewStorer(t)
	apiHandler := api.NewStrictHandler(NewAPI(db), nil)
	e := echo.New()
	api.RegisterHandlers(e, apiHandler)

	return db, e
}

func TestHappy(t *testing.T) {
	db, e := setup(t)

	db.EXPECT().
		FindPets(api.FindPetsParams{Tags: (*[]string)(nil), Limit: (*int32)(nil)}).
		Return([]api.Pet{
			{Id: 1, Name: "Hugo", Tag: &dog},
			{Id: 2, Name: "Corey", Tag: &cat},
			{Id: 3, Name: "Pepper", Tag: &dog},
		}, nil)
	req := httptest.NewRequest(http.MethodGet, "/pets", nil)
	rec := httptest.NewRecorder()
	e.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t,
		`[{"id":1,"name":"Hugo","tag":"dog"},{"id":2,"name":"Corey","tag":"cat"},{"id":3,"name":"Pepper","tag":"dog"}]`+"\n",
		rec.Body.String())
	assert.Equal(t, "/pets", req.URL.Path)

	db.EXPECT().
		AddPet("Hugo", &dog).
		Return(1, nil)
	body := `{"name":"Hugo","tag":"dog"}`
	req = httptest.NewRequest(http.MethodPost, "/pets", strings.NewReader(body))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec = httptest.NewRecorder()

	e.ServeHTTP(rec, req)
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, `{"id":1,"name":"Hugo","tag":"dog"}`+"\n", rec.Body.String())
	assert.Equal(t, "/pets", req.URL.Path)
}

func TestError(t *testing.T) {
	db, e := setup(t)

	db.EXPECT().
		AddPet("Hugo", &dog).
		Return(0, fmt.Errorf("UNIQUE constraint violated"))
	body := `{"name":"Hugo","tag":"dog"}`
	req := httptest.NewRequest(http.MethodPost, "/pets", strings.NewReader(body))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()

	e.ServeHTTP(rec, req)
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, `{"code":500,"message":"UNIQUE constraint violated"}`+"\n", rec.Body.String())
	assert.Equal(t, "/pets", req.URL.Path)

}
