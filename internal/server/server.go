// Package server implements the API server.
package server

import (
	"mime"
	"net/http"

	"github.com/labstack/echo-contrib/echoprometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"gitlab.com/lyda/template-go-api/internal/api"
	"gitlab.com/lyda/template-go-api/internal/static"
	"gitlab.com/lyda/template-go-api/internal/storage"
)

// Run creates and runs the server.
func Run(cmd *cobra.Command, _ []string) {
	// Get config values.
	host, err := cmd.Flags().GetString("listen")
	cobra.CheckErr(err)

	// Initialise all components.
	db := storage.New()
	defer db.Close()
	apiHandler := api.NewStrictHandler(NewAPI(db), nil)
	err = mime.AddExtensionType(".js", "application/javascript")
	cobra.CheckErr(err)
	err = mime.AddExtensionType(".json", "application/json")
	cobra.CheckErr(err)
	e := echo.New()
	logFormat := `${remote_ip} - - [${time_custom}] "${method} ${path} ${protocol}" ${status} ${bytes_out} "${referer}" "${user_agent}"` + "\n"
	customTimeFormat := "2/Jan/2006:15:04:05 -0700"
	logMiddleware := middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format:           logFormat,
		CustomTimeFormat: customTimeFormat,
	})

	// Register components with echo server.
	e.GET("/*", echo.WrapHandler(http.FileServer(http.FS(static.Files()))))
	api.RegisterHandlers(e, apiHandler)
	e.Use(logMiddleware)
	e.Use(echoprometheus.NewMiddleware("api"))
	e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	e.Logger.Fatal(e.Start(host))
}
