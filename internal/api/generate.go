// Package api implements the api (see api/api.yaml).
package api

//go:generate $OAPI_CODEGEN -config .models.yaml ../../api/api.yaml
//go:generate $OAPI_CODEGEN -config .endpoints.yaml ../../api/api.yaml
