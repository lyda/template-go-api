// Package storage stores application data.
package storage

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var duration = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Namespace: "api",
	Subsystem: "storage",
	Name:      "duration_seconds",
	Buckets:   []float64{.001, .01, .05, .1, .2, .5, 1},
}, []string{"action"})
