// Package storage stores application data.
package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/lyda/template-go-api/internal/api"

	"github.com/DATA-DOG/go-sqlmock"
)

var (
	dog = "dog"
	cat = "cat"
)

func setup(t *testing.T) (*DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	assert.Nil(t, err)
	//collector := sqlstats.NewStatsCollector("db_name", db)
	//prometheus.MustRegister(collector)
	return &DB{db: db}, mock
}

func TestAddPet(t *testing.T) {
	db, mock := setup(t)
	mock.ExpectExec("INSERT INTO pets").
		WithArgs("Hugo", &dog).
		WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := db.AddPet("Hugo", &dog)
	assert.Nil(t, err)
	assert.Equal(t, int64(1), id)
}

func TestDeletePet(t *testing.T) {
	db, mock := setup(t)
	mock.ExpectExec("DELETE FROM pets").
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))
	err := db.DeletePet(1)
	assert.Nil(t, err)
}

func TestFindPetByID(t *testing.T) {
	db, mock := setup(t)
	rows := sqlmock.NewRows([]string{"id", "name", "tag"}).
		AddRow(1, "Hugo", "dog")
	mock.ExpectQuery("SELECT id, name, tag FROM pets").WillReturnRows(rows)

	pet, err := db.FindPetByID(1)
	assert.Nil(t, err)
	assert.Equal(t, int64(1), pet.Id)
	assert.Equal(t, "Hugo", pet.Name)
}

func TestFindPets(t *testing.T) {
	db, mock := setup(t)
	rows := sqlmock.NewRows([]string{"id", "name", "tag"}).
		AddRow(1, "Hugo", "dog").
		AddRow(2, "Corey", "cat")
	mock.ExpectQuery("SELECT id, name, tag FROM pets").WillReturnRows(rows)
	pets, err := db.FindPets(api.FindPetsParams{})
	assert.Nil(t, err)
	assert.Equal(t, 2, len(pets))
}
