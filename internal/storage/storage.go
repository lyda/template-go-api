// Package storage stores application data.
package storage

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/dlmiddlecote/sqlstats"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"gitlab.com/lyda/template-go-api/internal/api"

	// This provides the driver needed by database/sql.
	_ "modernc.org/sqlite"
)

// DB contains the storage data and methods.
type DB struct {
	db *sql.DB
}

// New creates and runs the server.
func New() *DB {
	db, err := sql.Open("sqlite", ":memory:")
	cobra.CheckErr(err)
	collector := sqlstats.NewStatsCollector("db_name", db)
	prometheus.MustRegister(collector)
	_, err = db.Exec(`CREATE TABLE pets (
                        id INTEGER PRIMARY KEY,
						name TEXT,
						tag TEXT,
						UNIQUE(name)
					  )`)
	cobra.CheckErr(err)
	return &DB{db: db}
}

// Close releases the database.
func (db *DB) Close() {
	db.db.Close()
}

// AddPet saves a pet.
func (db *DB) AddPet(name string, tag *string) (int64, error) {
	timer := prometheus.NewTimer(duration.WithLabelValues("AddPet"))
	defer timer.ObserveDuration()
	res, err := db.db.Exec("INSERT INTO pets VALUES(NULL,?,?)", name, tag)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

// DeletePet saves a pet.
func (db *DB) DeletePet(id int64) error {
	timer := prometheus.NewTimer(duration.WithLabelValues("DeletePet"))
	defer timer.ObserveDuration()
	_, err := db.db.Exec("DELETE FROM pets WHERE id = ?", id)
	return err
}

// FindPetByID saves a pet.
func (db *DB) FindPetByID(id int64) (api.Pet, error) {
	timer := prometheus.NewTimer(duration.WithLabelValues("FindPetByID"))
	defer timer.ObserveDuration()
	var pet api.Pet
	err := db.db.QueryRow("SELECT id, name, tag FROM pets WHERE id = ?", id).
		Scan(&pet.Id, &pet.Name, &pet.Tag)
	return pet, err
}

func (db *DB) findPets(params api.FindPetsParams) (*sql.Rows, error) {
	query := "SELECT id, name, tag FROM pets"
	if params.Tags != nil && len(*params.Tags) > 0 {
		qs := strings.Repeat("?,", len(*params.Tags)-1) + "?"
		query = fmt.Sprintf("%s WHERE tags IN (%s)", query, qs)
		if params.Limit != nil && *params.Limit > 0 {
			query = fmt.Sprintf("%s LIMIT ?", query)
			return db.db.Query(query, *params.Tags, *params.Limit)
		}
		return db.db.Query(query, *params.Tags)
	}
	if params.Limit != nil && *params.Limit > 0 {
		query = fmt.Sprintf("%s LIMIT ?", query)
		return db.db.Query(query, *params.Limit)
	}
	return db.db.Query(query)
}

// FindPets saves a pet.
func (db *DB) FindPets(params api.FindPetsParams) ([]api.Pet, error) {
	timer := prometheus.NewTimer(duration.WithLabelValues("FindPets"))
	defer timer.ObserveDuration()
	rows, err := db.findPets(params)
	if err != nil {
		return []api.Pet{}, err
	}
	defer rows.Close()
	var pets []api.Pet
	for rows.Next() {
		var pet api.Pet
		err := rows.Scan(&pet.Id, &pet.Name, &pet.Tag)
		if err != nil {
			return []api.Pet{}, err
		}
		pets = append(pets, pet)
	}

	return pets, err
}
