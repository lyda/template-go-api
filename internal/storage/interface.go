// Package storage stores application data.
package storage

import (
	"gitlab.com/lyda/template-go-api/internal/api"
)

// Storer is the storage interface.
type Storer interface {
	Close()
	AddPet(name string, tag *string) (int64, error)
	DeletePet(id int64) error
	FindPetByID(id int64) (api.Pet, error)
	FindPets(params api.FindPetsParams) ([]api.Pet, error)
}
